#include <iostream>

using namespace std;

class node {
public:
	/*
		Keeping the data-members of a class 'public' is not considered a good
		habit. They are kept this way just to observe the corresponding syntax
		while the tree is constructed.
	*/
	int data;
	node* left;
	node* right;

	node(int data) {
		this->data=data;
		this->left=this->right=NULL;
	}

	/*
		If we comprehend the node itself to be the tree under the inspiration
		from the statement: "A tree has a root, a pointer to the left sub-tree,
		and a pointer to the right sub-tree.", We can perform the inorder
		traversal(or any functionality of our choice pertaining to the tree) of
		the tree from within the node class. This is as follows.
	*/
	void IO_print() {
		if(this->left!=NULL) this->left->IO_print();
		cout<<this->data<<" ";
		if(this->right!=NULL) this->right->IO_print();
	}
};

class node1 {
	//This time the data members of the class are kept private as they should be.
	int data;
	node1* left;
	node1* right;

public:
	node1(int data) {
		this->data=data;
		this->left=this->right=NULL;
	}

	/*
		Since the data-members are private, we need setters and getters to
		access and manipulate them.
	*/
	inline int get_data() { return this->data; }

	inline node1* get_left() { return this->left; }
	inline void set_left(node1* n) { this->left=n; }

	inline node1* get_right() { return this->right; }
	inline void set_right(node1* n) { this->right=n; }

	//Inorder Function is just like the one before in the 'node' class.
	void IO_print() {
		if(this->left!=NULL) this->left->IO_print();
		cout<<this->data<<" ";
		if(this->right!=NULL) this->right->IO_print();
	}
};

/*
	"A tree has some nodes."
	Picking up from this statement, we use the existing 'node1' class
	to form the nodes of the tree just because it is a better designed
	class following the recommendations of the Object-Orientation.
*/
class tree {
	node1 *root;
public:
	tree() { this->root=NULL; }
	
	inline node1* get_root() { return this->root; }
	inline void set_root(node1 *n) { this->root=n; }
	
	void IO_print(node1 *curr_node) {
		if(curr_node!=NULL) {
			this->IO_print(curr_node->get_left());
			cout<<curr_node->get_data()<<" ";
			this->IO_print(curr_node->get_right());
		}
	}
};

int main() {
	/*
							1
						/   \
					 2     3
				 /   \
				4     5
	*/
	// Tree constructed from the class 'node'.
	node* root = new node(1);
	root->left = new node(2);
	root->right= new node(3);
	root->left->left = new node(4);
	root->left->right = new node(5);
	root->IO_print();
	node root_1(2);
	root_1.IO_print();

	cout<<endl<<endl;

	/*
							10
						/    \
					 20    30
				 /    \
				40    50
	*/
	// Tree constructed from the class 'node1'
	node1* root1 = new node1(10);
	root1->set_left(new node1(20));
	root1->set_right(new node1(30));
	root1->get_left()->set_left(new node1(40));
	root1->get_left()->set_right(new node1(50));
	root1->IO_print();
	cout<<endl<<endl;

	/*
							100
						/     \
					 200    300
				 /     \
				400    500
	*/
	/*
		Tree constructed from the class 'tree' internally using the 'node1' class
		ignoring the InOrder function (aka IO_print) that is a part of 'node1' class.
	*/
	tree *t = new tree();
	t->set_root(new node1(100));
	t->get_root()->set_left(new node1(200));
	t->get_root()->set_right(new node1(300));
	t->get_root()->get_left()->set_left(new node1(400));
	t->get_root()->get_left()->set_right(new node1(500));
	t->IO_print(t->get_root());

	return 0;
}