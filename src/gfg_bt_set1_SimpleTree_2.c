#include <stdio.h>
#include <stdlib.h>

#define pln printf("\n");

typedef struct node{
	int d;
	struct node* l;
	struct node* r;
} node;

node* new_node(int data){
	node* n=(node*)malloc(sizeof(node));
	n->d=data;
	n->l=n->r=NULL;
	return n;
}

// Simulating OOP in C (Just a trailer not the whole movie)

int getd(node* n){ return n->d; }
void setd(node* n, int data){ n->d=data; }

node* getl(node* n){ return n->l; }
void setl(node* n, node* m){ n->l=m; }

node* getr(node* n){ return n->r; }
void setr(node* n, node* m){ n->r=m; }

void IO_print(node* curr_node){
	if(curr_node){
		IO_print(getl(curr_node));
		printf("%d",getd(curr_node));
		IO_print(getr(curr_node));
	}
}

int main(){
	node* root = new_node(1);
	
	setl(root,new_node(2)); // root.l
	setr(root,new_node(3)); // root.r

	setl(getl(root),new_node(4)); // root.l.l
	setl(getr(root),new_node(5)); // root.r.l

	IO_print(root); pln pln

	return 0;
}