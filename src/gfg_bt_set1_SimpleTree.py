'''The whole idea is to simply notice the variations in implementation of the same concept by
exploiting the syntax and object-orientation of the language.'''

# Node class that contains the data, the left pointer and the right pointer
class node(object):
	def __init__(self,data='NILL'):
		self.data=data
		self.left=self.right=None

	'''Consider the statement: "We can have a tree with a root, a pointer to the left subtree,
	and a pointer to the right subtree."
	With this perspective, we use the node as a tree itself and thus we perform the inorder
	from within the node class traversal'''
	def IO_print(self):
		if self.left is not None: self.left.IO_print()
		print(self.data,end=' ')
		if self.right is not None: self.right.IO_print()

'''Consider the statement: "A tree will have some nodes."
With this perspective, we use a separate "tree" class to represent/identify many trees.
And, we use the existing "node" class to fill up those trees. Thus we can perform the inorder
traversal from within the "tree" class.'''
class tree(object):
	def __init__(self):
		self.root=None

	# inorder function from within the tree class.
	def IO_print(self, curr_node):
		if curr_node:
			self.IO_print(curr_node.left)
			print(curr_node.data, end=' ')
			self.IO_print(curr_node.right)

# Aa usual, this is the global inorder function that can be applied to both node as well as tree classes
def IO_print(curr_node):
	if curr_node:
		IO_print(curr_node.left)
		print(curr_node.data, end=' ')
		IO_print(curr_node.right)

def main():
	root = node(1)
	root.left = node(2)
	root.right = node(3)
	root.left.left = node(4)
	root.right.left = node(5)
	print('root class\'s inorder traversal:\n\t', end=' '); root.IO_print(); print()

	Tree = tree()
	Tree.root = node(10)
	Tree.root.left = node(20)
	Tree.root.right = node(30)
	Tree.root.left.left = node(40)
	Tree.root.right.left = node(50)
	print('tree class\'s inorder traversal:\n\t', end=' '); Tree.IO_print(Tree.root); print()

	print('Global inorder for the node class: ',end=''); IO_print(root); print()
	print('Global inorder for the tree class: ',end=''); IO_print(Tree.root); print()

if __name__ == '__main__':
	main()


'''
OUTPUT:
prompt>python3 gfg_bt_set1_SimpleTree.py
root class's inorder traversal:
         4 2 1 5 3
tree class's inorder traversal:
         40 20 10 50 30
Global inorder for the node class: 4 2 1 5 3
Global inorder for the tree class: 40 20 10 50 30
'''