class node {
	private int d;
	private node l;
	private node r;

	node(int data) {
		this.d=data;
		this.l=this.r=null;
	}

	int getd(){ return this.d; }
	void setd(int data){ this.d=data; }

	node getl(){ return this.l; }
	void setl(node m){ this.l=m; }

	node getr(){ return this.r; }
	void setr(node m){ this.r=m; }

	void IO_print() {
		if(this.l!=null) this.l.IO_print();
		System.out.print(this.d+" ");
		if(this.r!=null) this.r.IO_print();
	}
}

class gfg_bt_set1_SimpleTree{
	void IO_print(node n){
		if(n!=null){
			IO_print(n.getl());
			System.out.print(n.getd() + " ");
			IO_print(n.getr());
		}
	}
	public static void main(String [] args){
		gfg_bt_set1_SimpleTree qwe=new gfg_bt_set1_SimpleTree();
		
		node root = new node(1);
		
		root.setl(new node(2)); // root.l = new node(2)
		root.setr(new node(3)); // root.r = new node(3)

		root.getl().setl(new node(4)); // root.l.l = new node(4)
		root.getr().setl(new node(5)); // root.r.l = new node(5)

		root.IO_print(); System.out.println();
		qwe.IO_print(root);
	}
}