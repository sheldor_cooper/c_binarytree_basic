/*

I took the most basic code from geeks-for-geeks (hence the file name: gfg...), and I tried to modify it to try whatever I 
had in my mind.

find the original code here [https://www.geeksforgeeks.org/binary-tree-set-1-introduction/]

      tree
      ----
       j <------------Root (Odin all father)
     /   \
    f      k  <-------Intermediate Node (Parent of Nodes below and Children of a node above) (Thor and Loki)
  /   \      \
 a     h      z <-----leaves (Humans)

*/


#include <stdio.h>
#include <stdlib.h>

#define pln printf("\n");

struct node {
	int data;
	struct node* left;
	struct node* right;
};

// Two different variations of the pseudo-constructor in C
// var-1 ----> I have a feeling this one is better in terms of memory mangement
struct node* new_node_1(struct node* node, int data){
	node = (struct node*)malloc(sizeof(struct node)); // Allocate memory
	node->data=data; // Fill data
	node->left = NULL; // NUllify the left Pointer
	node->right = NULL; // Nullify the right pointer
	return node;
}

// var-2

struct node* new_node_2(int data){
	struct node* temp = (struct node*)malloc(sizeof(struct node));
	temp->data = data;
	temp->left = temp->right = NULL;
	return temp;
}

// print in the inorder just to TEST if the tree is formed correctly.
void IO_print(struct node* curr_node){
	if(curr_node){
		IO_print(curr_node->left);
		printf("%d ",curr_node->data);
		IO_print(curr_node->right);
	}
}

int main() {
	//-----------TREE-1-------------------
	struct node* root1 = NULL;
	root1 = new_node_1(root1,1);

	root1->left = new_node_1(root1->left,2);
	root1->right= new_node_1(root1->right,3);

	root1->left->left = new_node_1(root1->left->left,4);
	/*
								1
						/				\
					 2          3
				 /   \      /   \
			 	4   NULL  NULL  NULL
  		/  \
		NULL NULL
	
	*/

	IO_print(root1); pln pln

	//-----------TREE-2-------------------
	struct node* root2 = NULL;
	root2 = new_node_2(1);

	root2->left = new_node_2(2);
	root2->right= new_node_2(3);

	root2->left->left = new_node_2(4);
	root2->right->left= new_node_2(5);
	/*
								1
						/				\
					 2          3
				 /   \      /   \
			 	4   NULL   5		NULL
  		/  \			 /	 \
		NULL NULL  NULL  NULL
	
	*/

	IO_print(root2);

	return 0;
}


/*
OUTPUT:

C:\Users\Tj-Envy\Documents\Programming\HE\Data Structures\Trees\Tree Basics\C_TreesBacis>gcc -Wall gfg_bt_set1_SimpleTree.c -o a

C:\Users\Tj-Envy\Documents\Programming\HE\Data Structures\Trees\Tree Basics\C_TreesBacis>a
4 2 1 3

4 2 1 5 3


*/