/*

      tree
      ----
       j <------------Root (Odin all father)
     /   \
    f      k  <-------Intermediate Node (Parent of Nodes below and Children of a node above)
  /   \      \
 a     h      z <-----leaves 

*/


#include <stdio.h>
#include <stdlib.h>

#define pln printf("\n");

typedef struct node{
	int d;
	struct node* l;
	struct node* r;
} node;

node* new_node_1(node* n, int data){
	n = (node*)malloc(sizeof(node));
	n->d = data;
	n->l = n->r = NULL;
	return n;
}

node* new_node_2(int data){
	node* n = (node*)malloc(sizeof(node));
	n->d = data;
	n->l = n->r = NULL;
	return n;
}

void IO_print(node* curr_node){
	if(curr_node){
		IO_print(curr_node->l);
		printf("%d",curr_node->d);
		IO_print(curr_node->r);
	}
}

int main(){
	//-----------TREE-1-------------------
	node* root = NULL;
	root = new_node_1(root,1);

	root->l = new_node_1(root->l,2);
	root->r = new_node_1(root->r,3);

	root->l->l = new_node_1(root->l->l,4);
	/*
								1
						/				\
					 2          3
				 /   \      /   \
			 	4   NULL  NULL  NULL
  		/  \
		NULL NULL
	
	*/

	IO_print(root); pln pln

	//-----------TREE-2-------------------
	node* root2 = NULL;
	root2 = new_node_2(1);

	root2->l = new_node_2(2);
	root2->r = new_node_2(3);

	root2->l->l = new_node_2(4);
	root2->r->l = new_node_2(5);
	/*
								1
						/				\
					 2          3
				 /   \      /   \
			 	4   NULL   5		NULL
  		/  \			 /	 \
		NULL NULL  NULL  NULL
	
	*/

	IO_print(root2);

	return 0;
}

/*

OUTPUT:

C:\Users\Tj-Envy\Documents\Programming\HE\Data Structures\Trees\Tree Basics\C_TreesBacis>gcc -Wall gfg_bt_set1_SimpleTree_1.c -o a

C:\Users\Tj-Envy\Documents\Programming\HE\Data Structures\Trees\Tree Basics\C_TreesBacis>a
4213

42153

*/